<?php
/**
 * This file is part of the Taco Projects.
 *
 * Copyright (c) 2004, 2013 Martin Takáč (http://martin.takac.name)
 *
 * For the full copyright and license information, please view
 * the file LICENCE that was distributed with this source code.
 *
 * PHP version 5.3
 *
 * @author     Martin Takáč (martin@takac.name)
 */


namespace Taco\Tools\Hockej\Core\Parsers;



use Taco\Tools\Hockej\Core\ParseException;
use Taco\Tools\Hockej\Core\SourceContext;
use Taco\Tools\Hockej\Core\Domains;
use SimpleXMLElement;


/**
 *	Obecný parser.
 */
class CommonXmlParser
{

	private $context;


	/**
	 * @param SourceContext $context Umístnění, kořene katalgu - umístění bootstrapu, catalog.hockej, a podobně.
	 */
	function __construct (SourceContext $context)
	{
		$this->context = $context;
	}



	/**
	 * Z xml předlohy vytvoří definici.
	 *
	 *		<task ref="package">
	 *			<type>zip</type>
	 *			<source ref="fileset.source"/>
	 *			<destination>
	 *				<path ref="dir.build"/>
	 *				<path class="path">temp/sample/data</path>
	 *				<file>package.zip</file>
	 *			</destination>
	 *		</task>
	 *
	 * @param $node
	 * @return object
	 */
	public function buildTaskDefinition(SimpleXMLElement $node, array $catalogs = array(), array $symbols = array())
	{
		$res = $this->buildOneTaskDefinition($node, $catalogs, $symbols);
		return $res->node;
	}



	/**
	 * Zpracování definice o importovaném katalogu.
	 */
	function buildCatalogDefinition($node)
	{
		$def = (object) array(
				'src' => (string)$this->assertAttr($node, 'src'),
				'name' => (string)$this->assertAttr($node, 'as'),
				'version' => (string)$this->assertAttr($node, 'version'),
				);

		return $def;
	}



	/**
	 * Povinný atribut.
	 *
	 * @param SimpleXMLElement $node Uzel, ze kterého se parsuje obsah.
	 * @param string $attr Jméno atributu.
	 *
	 * @throws ParseException
	 *
	 * @return value
	 */
	function assertAttr(SimpleXMLElement $node, $attr)
	{
		if (! isset($node[$attr])) {
			throw new ParseException("Required attr `$attr' is not set in: `" . substr($node->asXML(), 0, 250). "' in line: " . $this->getLineNo($node) . '.');
		}

		if (empty($node[$attr])) {
			throw new ParseException("Required attr `$attr' is empty in: `" . substr($node->asXML(), 0, 250). "' in line: " . $this->getLineNo($node) . '.');
		}

		return $node[$attr];
	}



	/**
	 * Umístění čísla řádky elementu v rámci celého DOMu.
	 *
	 * @param SimpleXMLElement $node Uzel, ze kterého se parsuje obsah.
	 *
	 * @return int
	 */
	function getLineNo(SimpleXMLElement $node)
	{
		$dom = dom_import_simplexml($node);
		return $dom->getLineNo();
	}



	public function registerNamespace(SimpleXMLElement $node)
	{
		$node->registerXPathNamespace('d', 'urn:hockej/documentation');
		//~ $node->registerXPathNamespace('contact', 'urn:nermal/contact');
		return $node;
	}



	private function buildOneTaskDefinition(SimpleXMLElement $node, array $catalogs = array(), array $symbols = array())
	{
		//~ $base = $node->attributes('urn:hockej/hockej');
		$docs = $node->attributes('urn:hockej/documentation');
//~ print_r($base);
		$optionRefs = array();
		$def = Domains\Task::create(
				(string)$node->getName(),
				$this->lookupFrom((string)$this->assertAttr($node, 'from'), $catalogs),
				self::colaesce('description', $docs, $node),
				isset($node['default']) ? (string)$node['default'] : Null,
				isset($node['return']) ? $this->lookupType((string)$node['return'], $catalogs, $symbols) : Null
				);
		$def->setSourceInfo($this->context, $node->asXml(), $this->getLineNo($node));

		//	Implementace
		//	<path from="self" implement-runtime="php" implement-bootstrap="bootstrap.php" description="Element pro odkazování cesty.">
		if (isset($node['implement-runtime'], $node['implement-bootstrap'])) {
			$def->runtime = new Domains\Runtime((string)$node['implement-runtime'],
					$this->context,
					(string)$node['implement-bootstrap']
					);
		}

		//	Reference
		if (isset($node['param-as'])) {
			$def->addRef((string)$node['param-as']);
		}

		//	Maximální počet prvků.
		if (isset($node['max-occurs'])) {
			$def->setMaxOccurs((int)$node['max-occurs']);
		}

		//	Minimální počet prvků.
		if (isset($node['min-occurs'])) {
			$def->setMinOccurs((int)$node['min-occurs']);
		}

		//	Potomci
		$content = Null;
		if ($node->count()) {
			$counts = array();
			foreach ($node as $name => $subnode) {
				$counts[] = $name;
				$el = $this->buildOneTaskDefinition($subnode, $catalogs, $symbols);
				$def->addOption($name, $el->node);

				foreach ($el->refs as $k => $v) {
					$tmp = array_count_values($counts);
					$optionRefs[$k] = $def->getName() . '/[' . ($tmp[$name] - 1). ']' . $v;
				}
			}
		}
		else {
			$content = (string) $node;
			if (strlen($content)) {
				$def->addOptionDefault($content);
			}
		}

		return (object) array(
				'node' => $def,
				'refs' => $optionRefs,
				);
	}



	/**
	 * Dohledá URI katalogu, ke kterému patří.
	 * @param string
	 * @return string
	 */
	private function lookupFrom($name, array $catalogs = array())
	{
		if ('self' == $name) {
			return $name;
		}

		if (! strpos($name, '.')) {
			return $name;
		}

		list($ns, $taskname) = explode('.', $name, 2);

		if (! isset($catalogs[$ns])) {
			throw new ParseException("Unknow task `{$name}', unknow namespace.");
		}

		return $catalogs[$ns]->src . ':' . $catalogs[$ns]->version . ':' . $taskname;
	}



	/**
	 * Dohledá URI katalogu, ke kterému patří.
	 * @param string
	 * @return Symbol | SymbolRef
	 */
	private function lookupType($name, array $catalogs = array(), array $symbols = array())
	{
		if ('self' == $name) {
			return $name;
		}

		if (! strpos($name, '.')) {
			if (! isset($symbols[$name])) {
				throw new ParseException("Unknow type `{$name}'. Not found in this context neither in any catalogs.");
			}
			return $symbols[$name];
		}

		list($ns, $taskname) = explode('.', $name, 2);

		if (! isset($catalogs[$ns])) {
			throw new ParseException("Unknow type `{$name}'. Not found in this context neither in any catalogs.");
		}

		return new Domains\SymbolRef($taskname, $catalogs[$ns]->src, $catalogs[$ns]->version, $ns);
	}



	private static function colaesce($name, $a1 = array(), $a2 = array(), $a3 = array())
	{
		if (isset($a1[$name])) {
			return (string) $a1[$name];
		}

		if (isset($a2[$name])) {
			return (string) $a2[$name];
		}

		if (isset($a3[$name])) {
			return (string) $a3[$name];
		}
	}


}
