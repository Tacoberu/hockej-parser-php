<?php
/**
 * This file is part of the Taco Projects.
 *
 * Copyright (c) 2004, 2013 Martin Takáč (http://martin.takac.name)
 *
 * For the full copyright and license information, please view
 * the file LICENCE that was distributed with this source code.
 *
 * PHP version 5.3
 *
 * @author     Martin Takáč (martin@takac.name)
 */


namespace Taco\Tools\Hockej\Core\Parsers;



use Taco\Tools\Hockej\Core\ParseException;
use Taco\Tools\Hockej\Core\SourceContext;
use Taco\Tools\Hockej\Core\Domains;



/**
 *	Název souboru
 */
class BuildscriptXmlParser implements BuildscriptParserInterface
{

	/**
	 *	Dekorovaná data jako xml.
	 */
	private $data;


	/**
	 *	Informace o zdroji, zda se jedná o soubor, nebo případně i něco jiného.
	 */
	private $context;


	/**
	 *	Sdílená logika parasování.
	 */
	private $commonBuilder;


	/**
	 * Zdroj definice katalogu.
	 *
	 * @param string $data String obsahující xml.
	 * @param SourceContext $context Umístnění, kořene katalgu - umístění bootstrapu, catalog.hockej, a podobně.
	 */
	function __construct ($data, SourceContext $context)
	{
		if (empty($data)) {
			throw new \InvalidArgumentException('Empty data.');
		}

		$this->context = $context;

		libxml_use_internal_errors(True);
		$this->data = simplexml_load_string($data);

		if (empty($this->data)) {
			$result = array();
			$errors = libxml_get_errors();
			$xmllines = explode("\n", $data);
			foreach ($errors as $error) {
				$result[] = self::formatXmlError($error, $xmllines);
			}
			libxml_clear_errors();

			throw new ParseException("Invalid format data\n" . implode(PHP_EOL, $result));
		}

		$this->commonBuilder = new CommonXmlParser($this->context);
	}



	/**
	 * Identifikace buildscriptu.
	 * @return
	 */
	public function getIdentification()
	{
		$res = (object)array();
		$res->id = (string)$this->data->id;
		$res->version = (string)$this->data->version;
		$res->description = (string)$this->data->description;
		$res->sourceinfo = $this->context;

		return $res;
	}



	/**
	 * Seznam globálních tasků.
	 *
	 * @return array of object
	 */
	public function getTasks()
	{
		$tasks = array();
		foreach ($this->data->catalog as $node) {
			if (! isset($node['from'])) {
				foreach ($node as $name => $subnode) {
					$tasks[$name] = $this->buildTaskDefinition($subnode);
				}
			}
		}

		return $tasks;
	}



	/**
	 * Seznam příkazů použitelných z CLI.
	 *
	 * @return array of object
	 */
	public function getCommands()
	{
		$ret = array();
		foreach ($this->data->command as $node) {
			$el = self::buildCommandsDefinition($node);

			if (isset($ret[$el->name])) {
				throw new \RuntimeException("Command with name '{$el->name}' is exist.");
			}

			$ret[] = $el;
		}

		return $ret;
	}



	/**
	 * Seznam importovaných katalogů. Zápis:
	 * <catalog src="url" version="1.2.3" as="t" />
	 *
	 * @return array of Catalogs
	 */
	public function getCatalogs()
	{
		$ret = array();
		foreach ($this->data->catalog as $node) {
			if (!isset($node['as'])) {
				continue;
			}

			$el = $this->commonBuilder->buildCatalogDefinition($node);
			$el->file = $this->context->getFile();
			if (isset($ret[$el->name])) {
				if ($el->name) {
					throw new \RuntimeException("Catalog with name '{$el->name}' is exist. In file: `{$this->context->getFile()->getRealPath()}'.");
				}
				else {
					throw new \RuntimeException("Catalog with local definitions is exist. Have you atribute 'as' by your importing catalog? In file: `{$this->context->getFile()->getRealPath()}'.");
				}
			}

			$ret[$el->name] = $el;
		}

		return $ret;
	}



	// --- Privates ------------------------------------------------------------



	/**
	 *	Naformátuje chyby včetně zdrojového kodu.
	 */
	private static function formatXmlError($error, array $xml)
	{
		$return  = strtr($xml[$error->line - 1], "\t", " ") . "\n";
		$return .= str_repeat('-', $error->column) . "^\n";

		switch ($error->level) {
		    case LIBXML_ERR_WARNING:
		        $return .= "Warning $error->code: ";
		        break;
		     case LIBXML_ERR_ERROR:
		        $return .= "Error $error->code: ";
		        break;
		    case LIBXML_ERR_FATAL:
		        $return .= "Fatal Error $error->code: ";
		        break;
		}

		$return .= trim($error->message) .
		           "\n  Line: $error->line" .
		           "\n  Column: $error->column";

		if ($error->file) {
		    $return .= "\n  File: $error->file";
		}

		return "$return\n\n--------------------------------------------\n\n";
	}



	/**
	 * Z xml předlohy vytvoří definici.
	 *
	 * <target name="path" description="dir.source">
	 *
	 *
	 * </target>
	 *
	 * @param $node
	 * @return object
	 */
	private function buildCommandsDefinition($node)
	{
		$def = Domains\Command::create((string)$node['name'], (string)$node['description']);
		$def->setSourceInfo($this->context, $node->asXml(), $this->commonBuilder->getLineNo($node));

		foreach ($node as $task) {
			$def->tasks[] = self::buildTaskDefinition($task, $this->getCatalogs());
		}

		return $def;
	}



	/**
	 * Z xml předlohy vytvoří definici.
	 *
	 *		<task ref="package">
	 *			<type>zip</type>
	 *			<source ref="fileset.source"/>
	 *			<destination>
	 *				<path ref="dir.build"/>
	 *				<path class="path">temp/sample/data</path>
	 *				<file>package.zip</file>
	 *			</destination>
	 *		</task>
	 *
	 * @param $node
	 * @return object
	 */
	private function buildTaskDefinition($node)
	{
		return $this->commonBuilder->buildTaskDefinition($node, $this->getCatalogs());
	}


}
