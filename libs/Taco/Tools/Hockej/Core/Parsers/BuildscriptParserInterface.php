<?php
/**
 * This file is part of the Taco Projects.
 *
 * Copyright (c) 2004, 2013 Martin Takáč (http://martin.takac.name)
 *
 * For the full copyright and license information, please view
 * the file LICENCE that was distributed with this source code.
 *
 * PHP version 5.3
 *
 * @author     Martin Takáč (martin@takac.name)
 */


namespace Taco\Tools\Hockej\Core\Parsers;



/**
 *	Parser poskytující definiční data o výrazech.
 */
interface BuildscriptParserInterface
{


	/**
	 * Identifikace buildscriptu.
	 * @return
	 */
	function getIdentification();


	/**
	 * Seznam tasků definovaných přímo ve scriptu. Neobsahují namespace. 
	 * Můžou být lokální.
	 *
	 * @return array of object
	 */
	function getTasks();



	/**
	 * Seznam commandů, což jsou příkazy spustitelné z CLI.
	 *
	 * @return array of object
	 */
	function getCommands();


	/**
	 * Seznam katalogů. Katalog obsahuje tasky a funguje částečně jako knihovna.
	 *
	 * @return array of Catalogs
	 */
	function getCatalogs();


}
