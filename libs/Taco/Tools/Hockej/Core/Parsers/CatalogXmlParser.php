<?php
/**
 * This file is part of the Taco Projects.
 *
 * Copyright (c) 2004, 2013 Martin Takáč (http://martin.takac.name)
 *
 * For the full copyright and license information, please view
 * the file LICENCE that was distributed with this source code.
 *
 * PHP version 5.3
 *
 * @author     Martin Takáč (martin@takac.name)
 */


namespace Taco\Tools\Hockej\Core\Parsers;


use Taco\Tools\Hockej\Core\ParseException,
	Taco\Tools\Hockej\Core\SourceContext,
	Taco\Tools\Hockej\Core\SplFileInfo,
	Taco\Tools\Hockej\Core\Domains;
use SimpleXMLElement;


/**
 *	Parsování catalogu.
 */
class CatalogXmlParser implements CatalogParserInterface
{

	/**
	 *	Dekorovaná data jako xml.
	 */
	private $data;


	/**
	 *	Umístění parsovaného souboru, pro umístění této informace do výpisu.
	 */
	private $context;


	private $runtime;


	/**
	 *	Sdílená logika parasování.
	 */
	private $commonBuilder;


	/**
	 * Zdroj definice katalogu.
	 *
	 * @param string $data String obsahující xml.
	 * @param SourceContext $context Umístnění, kořene katalgu - umístění bootstrapu, catalog.hockej, a podobně.
	 */
	function __construct ($data, SourceContext $context)
	{
		if (empty($data)) {
			throw new \InvalidArgumentException('Empty data.');
		}

		$this->context = $context;

		libxml_use_internal_errors(True);
		$this->data = simplexml_load_string($data);

		if (empty($this->data)) {
			$result = array();
			$errors = libxml_get_errors();
			$xmllines = explode("\n", $data);
			foreach ($errors as $error) {
				$result[] = self::formatXmlError($error, $xmllines);
			}
			libxml_clear_errors();

			throw new ParseException("Invalid format data\n" . implode(PHP_EOL, $result));
		}

		$this->commonBuilder = new CommonXmlParser($this->context);
	}



	/**
	 * Identifikace catalogu.
	 * @return
	 */
	public function getIdentification()
	{
		$this->data = $this->commonBuilder->registerNamespace($this->data);

		$res = (object)array();
		$res->id = (string)$this->data->id;
		$res->version = (string)$this->data->version;
		$res->categories = array();
		foreach ($this->data->category as $cat) {
			$res->categories[] = (string)$cat;
		}

		$res->description = (string)$this->data->description;
		$docs = $this->data->children('urn:hockej/documentation');
		if (isset($docs->description) && strlen($docs->description)) {
			$res->description = (string)$docs->description;
		}

		$schema = $this->data->children('urn:hockej/schema');
		$res->vendor = isset($schema->vendor) ? (string)$schema->vendor : Null;
		$res->package = isset($schema->package) ? (string)$schema->package : Null;

		//	Donahrát vendor a package ze starého způsobu.
		if (empty($res->vendor) || empty($res->package)) {
			if (empty($res->id) || ! strpos($res->id, '.')) {
				throw new ParseException("Invalid format id: `{$res->id}'.");
			}

			list($vendor, $package) = explode('.', $res->id);
			if (empty($res->vendor)) {
				$res->vendor = $vendor;
			}
			if (empty($res->package)) {
				$res->package = $package;
			}
		}

		$res->sourceinfo = $this->context;

		return $res;
	}



	/**
	 * Definice tasků a typů.
	 *
	 * @return array of ...
	 */
	public function getDefinitions()
	{
		$tasks = array();
		$symbols = $this->getSymbols();
		$catalogs = $this->getCatalogs();
		foreach ($this->data->catalog as $node) {
			if (! isset($node['from'])) {
				foreach ($node as $name => $subnode) {
					$tasks[$name] = $this->buildTaskDefinition($subnode, $catalogs, $symbols);
				}
			}
		}

		return $tasks;
	}



	/**
	 * Seznam importovaných katalogů. Zápis:
	 * <catalog src="url" version="1.2.3" as="t" />
	 *
	 * @return array of Catalogs
	 */
	public function getCatalogs()
	{
		$ret = array();
		foreach ($this->data->catalog as $node) {
			if (!isset($node['as'])) {
				continue;
			}

			$el = $this->commonBuilder->buildCatalogDefinition($node);
			$el->file = $this->context->getFile();

			if (isset($ret[$el->name])) {
				if ($el->name) {
					throw new \RuntimeException("Catalog with name '{$el->name}' is exist. In file: `{$this->context->getFile()->getRealPath()}'.");
				}
				else {
					throw new \RuntimeException("Catalog with local definitions is exist. Have you atribute 'as' by your importing catalog? In file: `{$this->context->getFile()->getRealPath()}'.");
				}
			}

			$ret[$el->name] = $el;
		}

		return $ret;
	}



	/**
	 * Seznam definovaných symbolů.
	 *
	 * @return array of Symbol
	 */
	public function getSymbols()
	{
		$result = array();

		$id = $this->getIdentification();

		foreach ($this->data->catalog as $node) {
			if (isset($node['as'])) {
				continue;
			}

			if (isset($node['src'])) {
				continue;
			}

			break;
		}

		foreach ($node->children('urn:hockej/schema') as $symbol) {
			$attrs = $symbol->attributes();
			$m = new Domains\Symbol($id->package, self::assertAttr($attrs, 'name'));
			foreach ($symbol->children('urn:hockej/schema') as $item) {
				switch ($item->getName()) {
					case 'implements':
						$a = $item->attributes();
						list($major, $minor) = explode('.', self::assertAttr($a, 'version'));
						$m->addVariant(self::assertAttr($a, 'vendor'), $major, $minor);
						break;
					case 'accessor':
						$m->addAccessor($this->parseSymbolAccessor($item));
						break;
				}
			}

			$result[$m->getName()] = $m;
		}

		return $result;
	}



	// --- Privates ------------------------------------------------------------



	private function parseSymbolImplement(SimpleXMLElement $node)
	{
		$attrs = $node->attributes();
		return (object) array(
				'vendor' => self::assertAttr($attrs, 'vendor'),
				'version' => self::assertAttr($attrs, 'version'),
				);
	}



	private function parseSymbolAccessor(SimpleXMLElement $node)
	{
		$attrs = $node->attributes();
		return self::assertAttr($attrs, 'name');
	}



	/**
	 *	Naformátuje chyby včetně zdrojového kodu.
	 */
	private static function formatXmlError($error, array $xml)
	{
		$return  = strtr($xml[$error->line - 1], "\t", " ") . "\n";
		$return .= str_repeat('-', $error->column) . "^\n";

		switch ($error->level) {
		    case LIBXML_ERR_WARNING:
		        $return .= "Warning $error->code: ";
		        break;
		     case LIBXML_ERR_ERROR:
		        $return .= "Error $error->code: ";
		        break;
		    case LIBXML_ERR_FATAL:
		        $return .= "Fatal Error $error->code: ";
		        break;
		}

		$return .= trim($error->message) .
		           "\n  Line: $error->line" .
		           "\n  Column: $error->column";

		if ($error->file) {
		    $return .= "\n  File: $error->file";
		}

		return "$return\n\n--------------------------------------------\n\n";
	}



	/**
	 * Z xml předlohy vytvoří definici.
	 *
	 * <target name="path" description="dir.source">
	 *
	 *
	 * </target>
	 *
	 * @param $node
	 * @return object
	 */
	private function buildTaskDefinition(SimpleXMLElement $node, array $catalogs = array(), array $symbols = array())
	{
		return $this->commonBuilder->buildTaskDefinition($node, $catalogs, $symbols);
	}



	/**
	 * @param SimpleXml
	 *
	 * 	any (*) libovoněkrát 0 - n
	 * 	once (.) právě jednou 1
	 * 	optional (?) jednou nebo vůbec 0 - 1
	 * 	some (+) jednou nebo vícekrát 1 - n
	 *
	 * @return string
	 */
	private static function formatCaller($param)
	{
		$min = 0;
		$max = 100;

		if (isset($param['min-occurs'])) {
			$min = (int) $param['min-occurs'];
		}

		if (isset($param['max-occurs'])) {
			$max = (int) $param['max-occurs'];
		}

		if ($max <= 0) {
			return Null;
		}

		$name = ($param['element'] == '#text') ? 'value' : $param['element'];
		if ($max > 1 && $max > $min) {
			return 'add' . ucfirst($name);
		}
		return 'set' . ucfirst($name);
	}



	/**
	 * Povinný atribut.
	 *
	 * @param $xml Uzel, ze kterého se parsuje obsah.
	 * @param string $attr Jméno atributu.
	 *
	 * @return value
	 */
	private static function assertAttr($xml, $attr)
	{
		if (! isset($xml[$attr])) {
			throw new ParseException("Required attr [$attr] is not set in [" . substr($xml->asXML(), 0, 250). "]");
		}

		return (string)$xml[$attr];
	}



	/**
	 * @param $xml Uzel, ze kterého se parsuje obsah.
	 * @param string $node Jméno uzlu.
	 *
	 * @return value
	 */
	private static function assertNode($xml, $node)
	{
		if (! isset($xml->$node)) {
			throw new ParseException("Node [$node] is not set in [" . substr($xml->asXML(), 0, 250). "]");
		}

		return $xml->$node;
	}



	private static function first($it, $default = Null)
	{
		foreach ($it as $value) {
			return (string)$value;
		}
		return $default;
	}


}
