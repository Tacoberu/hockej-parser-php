<?php
/**
 * This file is part of the Taco Projects.
 *
 * Copyright (c) 2004, 2013 Martin Takáč (http://martin.takac.name)
 *
 * For the full copyright and license information, please view
 * the file LICENCE that was distributed with this source code.
 *
 * PHP version 5.3
 *
 * @author     Martin Takáč (martin@takac.name)
 */

namespace Taco\Tools\Hockej\Core;

/**
 * Kontext určuje, kde jsme k tomu obsahu přišli.
 */
class SourceContext
{
	private $file;
	private $url;
	private $version;

    /**
     * Constructor
     *
     * @param string $file The file name
     */
    public function __construct(SplFileInfo $file, $url = Null, $version = Null)
    {
        $this->file = $file;
        $this->url = $url;
        $this->version = $version;
    }


    /**
     * Returns the file
     *
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }


    /**
     * Returns the url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }


    /**
     * Returns the version
     *
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }


    /**
     * @return string
     */
    public function createKey()
    {
        return $this->url . '/' . $this->version;
    }

}
