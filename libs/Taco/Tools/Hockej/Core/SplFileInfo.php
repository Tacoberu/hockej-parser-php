<?php
/**
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 * Copyright (c) since 2004 Martin Takáč
 * @author Martin Takáč <martin@takac.name>
 */

namespace Taco\Tools\Hockej\Core;

use RuntimeException;


/**
 * Extends \SplFileInfo to support relative paths
 *
 * @author Fabien Potencier <fabien@symfony.com>
 * @author Martin Takáč <martin@takac.name>
 */
class SplFileInfo extends \SplFileInfo
{

	/**
	 * @var string
	 */
    private $relativePath;


	static function fromSplFileInfo(\SplFileInfo $file)
	{
		return new self($file->getPathname(), $file->getPath() ?: '.');
	}



    /**
     * @param string $base             The absolute base name or filename.
     * @param string $relativePathname The relative path name or filename.
     */
	static function create($base, $relative)
	{
		if ($base && ! in_array($base{0}, [DIRECTORY_SEPARATOR, '\\', '/'], True)) {
            throw new RuntimeException("\$base must be absolute path. Missing start `/'.");
		}
		$path = array_filter([$base, $relative]);
		return new self(implode(DIRECTORY_SEPARATOR, $path), dirname($relative));
	}



    /**
     * @param string $file             The file name
     * @param string $relativePath     The relative path from $file.
     */
    function __construct($file, $relativePath)
    {
		if ($relativePath && $relativePath !== '.') {
			if (substr(dirname($file), -(strlen($relativePath))) !== $relativePath) {
				throw new RuntimeException("\$relativePath `$relativePath' is not correct for `$file'.");
			}
		}
        parent::__construct($file);
        $this->relativePath = $relativePath ?: false;
    }



    /**
     * Returns the relative path
     *
     * @return string the relative path
     */
    function getRelativePath()
    {
        return $this->relativePath;
    }



    /**
     * Returns the relative path name
     *
     * @return string the relative path name
     */
    function getRelativePathname()
    {
		if ( ! $this->getRelativePath()) {
			return False;
		}
        return $this->getRelativePath() . DIRECTORY_SEPARATOR . $this->getFilename();
    }



    /**
     * Returns the contents of the file
     *
     * @return string the contents of the file
     *
     * @throws \RuntimeException
     */
    function getContents()
    {
        $level = error_reporting(0);
        $content = file_get_contents($this->getPathname());
        error_reporting($level);
        if (false === $content) {
            $error = error_get_last();
            throw new RuntimeException($error['message']);
        }

        return $content;
    }
}
