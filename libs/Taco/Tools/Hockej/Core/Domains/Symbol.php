<?php
/**
 * This file is part of the Taco Projects.
 *
 * Copyright (c) 2004, 2013 Martin Takáč (http://martin.takac.name)
 *
 * For the full copyright and license information, please view
 * the file LICENCE that was distributed with this source code.
 *
 * PHP version 5.3
 *
 * @author     Martin Takáč (martin@takac.name)
 */


namespace Taco\Tools\Hockej\Core\Domains;


use Taco\Tools\Hockej\Core\ParseException;
use Taco\Tools\Hockej\Core\NObject;
use Taco\Tools\Hockej\Core\SourceContext;
use JsonSerializable;


/**
 *	typ.
 */
class Symbol extends NObject implements JsonSerializable
{

	/**
	 * @var string
	 */
	private $name;



	/**
	 * @var string
	 */
	private $package;



	/**
	 * Seznam verzí implementací, které to implementuje.
	 * @var array
	 */
	private $variants = array();



	/**
	 * Seznam funkcí, které umí pracovat s blobem.
	 * @var array
	 */
	private $accessors = array();



	/**
	 * ?
	 */
	function __construct($package, $name)
	{
		$this->package = $package;
		$this->name = $name;
	}



	/**
	 * Implementjící verzi
	 */
	function addVariant($vendor, $major, $minor)
	{
		$this->variants[] = (object) array(
				'vendor' => $vendor,
				'major' => $major,
				'minor' => $minor
				);
	}



	/**
	 * Tento symbol vyžaduje metodu
	 */
	function addAccessor($m)
	{
		$this->accessors[] = $m;
	}



	/**
	 * Orientační reprezentace
	 * @return string
	 */
	function __toString()
	{
		return $this->getType()
			. '{' . implode(',', $this->accessors) . '}';
	}



	/**
	 * JSON verze.
	 * @return string
	 */
	function jsonSerialize()
	{
		return (string)$this;
	}



	/**
	 * Type ident, without functions.
	 * @return string
	 */
	function getType()
	{
		return $this->getFullname() . '/' . implode(';', $this->getVariants());
	}



	/**
	 * Type ident, without functions.
	 * @return string
	 */
	function getName()
	{
		return $this->name;
	}



	/**
	 * Type ident, without functions.
	 * @return string
	 */
	function getFullname()
	{
		return $this->package . '.' . $this->name;
	}



	/**
	 * @return string
	 */
	function getPackage()
	{
		return $this->package;
	}


	/**
	 * Type ident, without functions.
	 * @return array of string
	 */
	function getVariants()
	{
		return array_map(function($m) {
				return "{$m->vendor}:{$m->major}:{$m->minor}";
			}, $this->variants);
	}

}
