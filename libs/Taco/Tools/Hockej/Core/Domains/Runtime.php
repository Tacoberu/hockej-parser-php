<?php
/**
 * This file is part of the Taco Projects.
 *
 * Copyright (c) 2004, 2013 Martin Takáč (http://martin.takac.name)
 *
 * For the full copyright and license information, please view
 * the file LICENCE that was distributed with this source code.
 *
 * PHP version 5.3
 *
 * @author     Martin Takáč (martin@takac.name)
 */


namespace Taco\Tools\Hockej\Core\Domains;



use Taco\Tools\Hockej\Core\ParseException;
use Taco\Tools\Hockej\Core\SourceContext;



/**
 *	Implementace chování.
 */
class Runtime
{

	/**
	 *	Jazyk, atd: php, python, java.
	 */
	public $runtime;



	/**
	 *	Umístění catalogu.
	 */
	public $context;



	/**
	 *	Co ze zavolá. Přípojný bod.
	 */
	public $bootstrap;



	/**
	 * @param string $runtime As php, python, lua, bin
	 * @param SourceContext $context
	 * @param string $bootstrap Bootstrap file path from directory of package.
	 */
	public function __construct($runtime, SourceContext $context, $bootstrap)
	{
		$this->runtime = $runtime;
		$this->context = $context;
		$this->bootstrap = $bootstrap;
	}


	function createKey()
	{
		return $this->runtime
				. ':' . $this->context->createKey()
				. '/' . $this->bootstrap;
				;
	}

}
