<?php
/**
 * This file is part of the Taco Projects.
 *
 * Copyright (c) 2004, 2013 Martin Takáč (http://martin.takac.name)
 *
 * For the full copyright and license information, please view
 * the file LICENCE that was distributed with this source code.
 *
 * PHP version 5.3
 *
 * @author     Martin Takáč (martin@takac.name)
 */


namespace Taco\Tools\Hockej\Core\Domains;


use Taco\Tools\Hockej\Core\ParseException;
use Taco\Tools\Hockej\Core\NObject;
use Taco\Tools\Hockej\Core\SourceContext;


/**
 *	Pracovník.
 */
class Task extends NObject
{

	/**
	 * Jméno příkazu.
	 * @var string
	 */
	private $name;



	/**
	 * Z čeho je to porobek?
	 * @var string
	 */
	private $extend;



	/**
	 * Popisek
	 * @var string
	 */
	private $description;



	/**
	 * Který option se nastaví, v případě <tag>value</tag>
	 * @var string
	 */
	private $defaultOption = Null;



	/**
	 * Typ návratové hodnoty.
	 * @var string
	 */
	private $return = Null;



	/**
	 * Minimální počet optionů. Defaultně 0.
	 * @val int
	 */
	private $minOccurs = 0;



	/**
	 * Maximální počet optionů. Defaultně Null = neomezeně.
	 * @val int
	 */
	private $maxOccurs = Null;



	/**
	 *	Nastavení.
	 */
	private $opts;



	/**
	 *	Hloubkové nastavení.
	 */
	private $refs = array();



	/**
	 *	Implementace chování.
	 */
	private $runtime = Null;



	/**
	 *	Informace o zdroji.
	 */
	private $sourceinfo = Null;



	/**
	 * @param string $name
	 * @param string $extend
	 * @param string $description
	 */
	private function __construct($name, $extend, $description)
	{
		$this->name = $name;
		$this->extend = $extend;
		$this->description = $description;
	}



	/**
	 * @param string $name Jméno tasku/uzlu
	 * @param string $extend Předloha, odvození nového tasku z této předlohy.
	 * @param string $description Doc
	 * @param string $default Na který option se převede literal.
	 * @param string $return jakého je typu návratová hodnota.
	 *
	 * @return self
	 */
	public static function create($name, $extend, $description, $default = Null, $return = Null)
	{
		$inst = new self($name, $extend, $description);
		$inst->defaultOption = $default;
		$inst->return = $return;
		return $inst;
	}



	/**
	 * Kontext určuje, kde jsme k tomu obsahu přišli.
	 */
	public function setSourceInfo(SourceContext $context, $code, $line)
	{
		return $this->sourceinfo = (object) array(
				'context' => $context,
				'code' => $code,
				'line' => $line,
				);
	}



	/**
	 * Informace o zdroji, ze kterého jsem tento task vytvořili.
	 * @return SourceContext
	 */
	public function getSourceInfo()
	{
		return $this->sourceinfo;
	}



	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}



	/**
	 * @return string
	 */
	public function getExtend()
	{
		return $this->extend;
	}


	/**
	 * @return string
	 */
	public function getReturnType()
	{
		return $this->return;
	}



	/**
	 * @return string
	 */
	public function getDescription()
	{
		return $this->description;
	}



	/**
	 * Který option se nastaví, v případě <tag>value</tag>
	 * @return string
	 */
	public function getDefaultOptionName()
	{
		return $this->defaultOption;
	}



	/**
	 * Minimální počet optionů. Defaultně 0.
	 * @return int
	 */
	public function getMinOccurs()
	{
		return $this->minOccurs;
	}



	/**
	 * Minimální počet optionů. Defaultně 0.
	 * @param int
	 */
	public function setMinOccurs($m)
	{
		$this->minOccurs = (int)$m;
		return $this;
	}



	/**
	 * Maximální počet optionů. Defaultně Null.
	 * @return int
	 */
	public function getMaxOccurs()
	{
		return $this->maxOccurs;
	}



	/**
	 * Maximální počet optionů. Defaultně Null.
	 * @param int
	 */
	public function setMaxOccurs($m)
	{
		$m = (int)$m;
		$this->maxOccurs = $m > 0 ? $m : Null;
		return $this;
	}



	public function setRuntime($runtime)
	{
		$this->runtime = $runtime;
		return $this;
	}



	public function getRuntime()
	{
		return $this->runtime;
	}



	/**
	 * Přiřazení hodnoty nastavení, když neznáme jméno optionu.
	 */
	public function addOptionDefault($value)
	{
		return $this->addOption('default', $value);
	}



	/**
	 * Přiřazení hodnoty nastavení.
	 */
	public function addOption($name, $value)
	{
		if (empty($this->opts)) {
			$this->opts = new \stdClass;
		}

		if (!isset($this->opts->$name)) {
			$this->opts->$name = array();
		}

		$this->opts->{$name}[] = $value;
		return $this;
	}


	public function getOpts()
	{
		return $this->opts;
	}


	public function getOptions()
	{
		return $this->opts;
	}


	public function hasOptions()
	{
		return ! empty($this->opts);
	}



	/**
	 * Přiřazení reference
	 */
	public function addRef($name)
	{
		$this->refs[] = $name;
		return $this;
	}



	public function getRefs()
	{
		return $this->refs;
	}



	public function hasRefs()
	{
		return ! empty($this->refs);
	}



	public function export()
	{
		if ($this->opts) {
			$opts = array();
			foreach ($this->opts as $param => $values) {
				if (! isset($opts[$param])) {
					$opts[$param] = array();
				}
				foreach ($values as $value) {
					if ($value instanceof self) {
						$opts[$param][] = $value->export();
					}
					else {
						$opts[$param][] = $value;
					}
				}
			}
		}

		return (object) array(
				'name' => $this->name,
				'extend' => $this->extend,
				'description' => $this->description,
				'opts' => isset($opts) ? $opts : Null,
				'refs' => $this->refs,
				'runtime' => $this->runtime,
				);
	}

}
