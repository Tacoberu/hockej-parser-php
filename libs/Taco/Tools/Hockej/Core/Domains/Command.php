<?php
/**
 * This file is part of the Taco Projects.
 *
 * Copyright (c) 2004, 2013 Martin Takáč (http://martin.takac.name)
 *
 * For the full copyright and license information, please view
 * the file LICENCE that was distributed with this source code.
 *
 * PHP version 5.3
 *
 * @author     Martin Takáč (martin@takac.name)
 */


namespace Taco\Tools\Hockej\Core\Domains;



use Taco\Tools\Hockej\Core\ParseException;
use Taco\Tools\Hockej\Core\SourceContext;



/**
 *	Akce hockeje.
 */
class Command
{

	/**
	 *	Jméno příkazu.
	 */
	public $name;


	/**
	 *	Popisek
	 */
	public $description;


	/**
	 *	Obsahuje tasky.
	 */
	public $tasks = array();


	/**
	 *	Informace o zdroji.
	 */
	private $sourceinfo = Null;


	public function __construct($name, $description)
	{
		$this->name = $name;
		$this->description = $description;
	}



	/**
	 * Vytvoření s pomocí 
	 */
	public static function create($name, $description)
	{
		return new self($name, $description);
	}



	public function setSourceInfo(SourceContext $context, $code, $line)
	{
		return $this->sourceinfo = (object) array(
				'context' => $context,
				'code' => $code,
				'line' => $line,
				);
	}



	public function getSourceInfo()
	{
		return $this->sourceinfo;
	}


	public function export()
	{
		return (object) array(
				'name' => $this->name,
				'description' => $this->description,
				'tasks' => $this->tasks,
				);
	}

}
