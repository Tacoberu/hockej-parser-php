<?php
/**
 * This file is part of the Taco Projects.
 *
 * Copyright (c) 2004, 2013 Martin Takáč (http://martin.takac.name)
 *
 * For the full copyright and license information, please view
 * the file LICENCE that was distributed with this source code.
 *
 * PHP version 5.3
 *
 * @author     Martin Takáč (martin@takac.name)
 */


namespace Taco\Tools\Hockej\Core\Domains;


use Taco\Tools\Hockej\Core\ParseException;
use Taco\Tools\Hockej\Core\NObject;
use Taco\Tools\Hockej\Core\SourceContext;
use JsonSerializable;


/**
 *	Odkaz na Symbol v nějakém katalogu.
 */
class SymbolRef extends NObject // implements JsonSerializable
{

	/**
	 * Jméno symbolu
	 * @var string
	 */
	private $name;



	/**
	 * URI catalogu.
	 * @var string
	 */
	private $catalog;



	/**
	 * Verze catalogu.
	 * @var string
	 */
	private $version;



	/**
	 * Pod jakým namespace byl catalog nalinkován.
	 * @var string
	 */
	private $ns;



	/**
	 * @param string $taskname
	 * @param string $catalog
	 * @param string $version
	 * @param string $ns
	 */
	function __construct($symbol, $catalog, $version, $ns)
	{
		$this->name = $symbol;
		$this->catalog = $catalog;
		$this->version = $version;
		$this->ns = $ns;
	}



	/**
	 * Orientační reprezentace
	 * @return string
	 */
	function __toString()
	{
		return $this->catalog . ':' . $this->version . '#' . $this->name;
	}


	/**
	 * symbol name
	 * @return string
	 */
	function getName()
	{
		return $this->name;
	}



	/**
	 * Katalog, ze kterého symbol taháme
	 * @return string
	 */
	function getCatalog()
	{
		return $this->catalog;
	}


	/**
	 * Verze katalogu.
	 * @return string
	 */
	function getVersion()
	{
		return $this->version;
	}


	/**
	 * Použite namespace
	 * @return string
	 */
	function getNs()
	{
		return $this->ns;
	}

}
