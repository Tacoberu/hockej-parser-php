<?php
/**
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 * Copyright (c) since 2004 Martin Takáč
 * @author Martin Takáč <martin@takac.name>
 */

namespace Taco\Tools\Hockej\Core;

use PHPUnit_Framework_TestCase;


/**
 * @call phpunit SourceContextTest.php
 */
class SourceContextTest extends PHPUnit_Framework_TestCase
{


	function testSimple()
	{
		$file = new SplFileInfo('aoo/foo/readme.md', 'foo');
		$m = new SourceContext($file);
		$this->assertEquals('aoo/foo/readme.md', (string)$m->getFile());
		$this->assertNull($m->getUrl());
		$this->assertNull($m->getVersion());
	}



	function testSimpleWithUrlAndVersion()
	{
		$file = new SplFileInfo('aoo/foo/readme.md', 'foo');
		$m = new SourceContext($file, 'abc', 'bbbb');
		$this->assertEquals('aoo/foo/readme.md', (string)$m->getFile());
		$this->assertEquals('abc', $m->getUrl());
		$this->assertEquals('bbbb', $m->getVersion());
	}

}
