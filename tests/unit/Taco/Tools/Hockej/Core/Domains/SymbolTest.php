<?php
/**
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 * Copyright (c) since 2004 Martin Takáč
 * @author Martin Takáč <martin@takac.name>
 */

namespace Taco\Tools\Hockej\Core\Domains;

use PHPUnit_Framework_TestCase;


/**
 * @call phpunit SymbolTest.php
 */
class SymbolTest extends PHPUnit_Framework_TestCase
{


	/**
	 * "hockej:core.numeric:0:1 taco:core.numeric:0:3",
	 */
	function testToString()
	{
		$m = new Symbol('core', 'numeric');
		$m->addVariant('hockej', 0, 1);
		$m->addVariant('taco', 0, 3);
		$m->addAccessor('numeric');
		$m->addAccessor('plus');
		$m->addAccessor('minus');
		$this->assertEquals('core.numeric/hockej:0:1;taco:0:3{numeric,plus,minus}', (string)$m);
	}



	/**
	 * Tedy musí být splněný všechny vendor, a minor být stejné, nebo nižší (v libovolné kombinaci).
	 * major verze odděluje
	 */
	function testEquals()
	{
	}



	function testJson()
	{
		$m = new Symbol('core', 'numeric');
		$m->addVariant('hockej', 0, 1);
		$m->addVariant('taco', 0, 3);
		$m->addAccessor('numeric');
		$m->addAccessor('plus');
		$m->addAccessor('minus');
		$this->assertEquals('"core.numeric\/hockej:0:1;taco:0:3{numeric,plus,minus}"', json_encode($m));
	}



	function testType()
	{
		$m = new Symbol('core', 'numeric');
		$m->addVariant('hockej', 0, 1);
		$m->addVariant('taco', 0, 3);
		$m->addAccessor('numeric');
		$m->addAccessor('plus');
		$m->addAccessor('minus');
		$this->assertEquals('core.numeric/hockej:0:1;taco:0:3', $m->type);
	}



	function testName()
	{
		$m = new Symbol('core', 'numeric');
		$m->addVariant('hockej', 0, 1);
		$m->addVariant('taco', 0, 3);
		$m->addAccessor('numeric');
		$m->addAccessor('plus');
		$m->addAccessor('minus');
		$this->assertEquals('numeric', $m->getName());
		$this->assertEquals('numeric', $m->name);
		$this->assertEquals('core', $m->package);
		$this->assertEquals('core.numeric', $m->fullname);
	}



	function testVariants()
	{
		$m = new Symbol('core', 'numeric');
		$m->addVariant('hockej', 0, 1);
		$m->addVariant('taco', 0, 3);
		$m->addAccessor('numeric');
		$m->addAccessor('plus');
		$m->addAccessor('minus');

		$this->assertEquals(array('hockej:0:1', 'taco:0:3'), $m->variants);
	}


}
