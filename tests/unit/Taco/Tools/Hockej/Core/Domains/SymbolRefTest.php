<?php
/**
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 * Copyright (c) since 2004 Martin Takáč
 * @author Martin Takáč <martin@takac.name>
 */

namespace Taco\Tools\Hockej\Core\Domains;

use PHPUnit_Framework_TestCase;


/**
 * @call phpunit SymbolRefTest.php
 */
class SymbolRefTest extends PHPUnit_Framework_TestCase
{

	function testMinimal()
	{
		$m = new SymbolRef('numeric', 'hockej://build-in/hockej/core', '0.1', 'a');
		$this->assertEquals('numeric', $m->name);
		$this->assertEquals('hockej://build-in/hockej/core', $m->catalog);
		$this->assertEquals('0.1', $m->version);
		$this->assertEquals('a', $m->ns);
		$this->assertEquals('hockej://build-in/hockej/core:0.1#numeric', (string)$m);
	}


}
