<?php
/**
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 * Copyright (c) since 2004 Martin Takáč
 * @author Martin Takáč <martin@takac.name>
 */

namespace Taco\Tools\Hockej\Core\Domains;

use PHPUnit_Framework_TestCase;
use Taco\Tools\Hockej\Core;


/**
 * @call phpunit TaskTest.php
 */
class TaskTest extends PHPUnit_Framework_TestCase
{

	function testMinimal()
	{
		$m = Task::create('foo', 'self', 'Lorem ipsum.');
		$this->assertEquals('foo', $m->name);
		$this->assertEquals('self', $m->extend);
		$this->assertEquals('Lorem ipsum.', $m->description);
		$this->assertNull($m->defaultOptionName);
		$this->assertNull($m->returnType);
		$this->assertEquals(0, $m->minOccurs);
		$this->assertNull($m->maxOccurs);
		$this->assertNull($m->options);
		$this->assertEquals(array(), $m->refs);
		$this->assertNull($m->runtime);
		$this->assertNull($m->sourceInfo);

		$this->assertEquals((object)array(
				'name' => 'foo',
				'extend' => 'self',
				'description' => 'Lorem ipsum.',
				'opts' => Null,
				'refs' => array(),
				'runtime' => Null,
				), $m->export());
	}


	function testMaxCreate()
	{
		$m = Task::create('foo', 'self', 'Lorem ipsum.', 'value', 'numeric');
		$this->assertEquals('foo', $m->name);
		$this->assertEquals('self', $m->extend);
		$this->assertEquals('Lorem ipsum.', $m->description);
		$this->assertEquals('value', $m->defaultOptionName);
		$this->assertEquals('numeric', $m->returnType);
		$this->assertEquals(0, $m->minOccurs);
		$this->assertNull($m->maxOccurs);
		$this->assertNull($m->options);
		$this->assertEquals(array(), $m->refs);
		$this->assertNull($m->runtime);
		$this->assertNull($m->sourceInfo);

		$this->assertEquals((object)array(
				'name' => 'foo',
				'extend' => 'self',
				'description' => 'Lorem ipsum.',
				'opts' => Null,
				'refs' => array(),
				'runtime' => Null,
				), $m->export());
	}



	private function createSourceContext()
	{
		$file = new Core\SplFileInfo('aoo/foo/readme.md', 'foo');
		$m = new Core\SourceContext($file);
		return $m;
	}

}
