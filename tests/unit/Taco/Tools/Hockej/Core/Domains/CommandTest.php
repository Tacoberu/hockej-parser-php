<?php
/**
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 * Copyright (c) since 2004 Martin Takáč
 * @author Martin Takáč <martin@takac.name>
 */

namespace Taco\Tools\Hockej\Core\Domains;

use PHPUnit_Framework_TestCase;


/**
 * @call phpunit CommandTest.php
 */
class CommandTest extends PHPUnit_Framework_TestCase
{


	function testSimple()
	{
		$m = new Command('foo', 'Lorem ipsum');
		$this->assertEquals('foo', $m->name);
		$this->assertEquals('Lorem ipsum', $m->description);
		$this->assertEquals((object)array(
				'name' => 'foo',
				'description' => 'Lorem ipsum',
				'tasks' => array()
				), $m->export());
	}



	function testWithCreateShortcut()
	{
		$m = Command::create('foo', 'Lorem ipsum');
		$this->assertEquals('foo', $m->name);
		$this->assertEquals('Lorem ipsum', $m->description);
		$this->assertEquals((object)array(
				'name' => 'foo',
				'description' => 'Lorem ipsum',
				'tasks' => array()
				), $m->export());
	}


}
