<?php
/**
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 * Copyright (c) since 2004 Martin Takáč
 * @author Martin Takáč <martin@takac.name>
 */

namespace Taco\Tools\Hockej\Core\Domains;

use PHPUnit_Framework_TestCase;
use Taco\Tools\Hockej\Core;


/**
 * @call phpunit RuntimeTest.php
 */
class RuntimeTest extends PHPUnit_Framework_TestCase
{

	private function createSourceContext()
	{
		$file = new Core\SplFileInfo('aoo/foo/readme.md', 'foo');
		$m = new Core\SourceContext($file);
		return $m;
	}



	function testSimple()
	{
		$m = new Runtime('php', $sc = $this->createSourceContext(), 'foo/bootstrap.phar');
		$this->assertEquals('php', $m->runtime);
		$this->assertEquals($sc, $m->context);
		$this->assertEquals('foo/bootstrap.phar', $m->bootstrap);
		$this->assertEquals('php://foo/bootstrap.phar', $m->createKey());
	}


}
