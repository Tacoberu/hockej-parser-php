<?php
/**
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 * Copyright (c) since 2004 Martin Takáč
 * @author Martin Takáč <martin@takac.name>
 */

namespace Taco\Tools\Hockej\Core;

use PHPUnit_Framework_TestCase;
use RuntimeException;


class SplFileInfoTest extends PHPUnit_Framework_TestCase
{

	function testConstructor()
	{
		$file = new SplFileInfo('a/b/c/d/readme.md', 'd');
		$this->assertSame('a/b/c/d', $file->getPath());
		$this->assertSame('a/b/c/d/readme.md', $file->getPathname());
		$this->assertSame('d', $file->getRelativePath());
		$this->assertSame('d/readme.md', $file->getRelativePathname());
	}



	function testCreateRelativeIsFail()
	{
		$this->setExpectedException(RuntimeException::class, '$base must be absolute path. Missing start `/\'.');
		SplFileInfo::create('a/b/c', 'd/readme.md');
	}



	function testInconsistentRelativePathIsFail()
	{
		$this->setExpectedException(RuntimeException::class, '$relativePath `b/c\' is not correct for `a/b/c/d/readme.md\'.');
		$file = new SplFileInfo('a/b/c/d/readme.md', 'b/c');
	}



	function testCreate()
	{
		$file = SplFileInfo::create('/a/b/c', 'd/readme.md');
		$this->assertSame('/a/b/c/d', $file->getPath());
		$this->assertSame('/a/b/c/d/readme.md', $file->getPathname());
		$this->assertSame('d', $file->getRelativePath());
		$this->assertSame('d/readme.md', $file->getRelativePathname());
	}



	function testCreateOnlyFilename()
	{
		$file = SplFileInfo::create('/a/b/c', 'readme.md');
		$this->assertSame('/a/b/c', $file->getPath());
		$this->assertSame('/a/b/c/readme.md', $file->getPathname());
		$this->assertSame('.', $file->getRelativePath());
		$this->assertSame('./readme.md', $file->getRelativePathname());
		$this->assertSame('/a/b/c/readme.md', (string)$file);
	}



	function testCreateWithoutFile()
	{
		$file = SplFileInfo::create('/a/b/c', 'd/e/');
		$this->assertSame('/a/b/c/d', $file->getPath());
		$this->assertSame('/a/b/c/d/e', $file->getPathname());
		$this->assertSame('d', $file->getRelativePath()); // adresář bere jako soubor
		$this->assertSame('d/e', $file->getRelativePathname());
	}



	function testCreateOnlyRelative()
	{
		$file = SplFileInfo::create(null, 'd/readme.md');
		$this->assertSame('d', $file->getPath());
		$this->assertSame('d/readme.md', $file->getPathname());
		$this->assertSame('d', $file->getRelativePath());
		$this->assertSame('d/readme.md', $file->getRelativePathname());
	}



	function testCreateOnlyAbsolute()
	{
		$file = SplFileInfo::create('/a/b/c/d/readme.md', null);
		$this->assertSame('/a/b/c/d', $file->getPath());
		$this->assertSame('/a/b/c/d/readme.md', $file->getPathname());
		$this->assertSame(False, $file->getRelativePath());
		$this->assertSame(False, $file->getRelativePathname());
	}



	function testCreateOnlyFile()
	{
		$file = SplFileInfo::create(null, 'readme.md');
		$this->assertSame('', $file->getPath());
		$this->assertSame('readme.md', $file->getPathname());
		$this->assertSame('.', $file->getRelativePath());
		$this->assertSame('./readme.md', $file->getRelativePathname());
	}

}
