<?php
/**
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 * Copyright (c) since 2004 Martin Takáč
 * @author Martin Takáč <martin@takac.name>
 */

namespace Taco\Tools\Hockej\Core\Parsers;

use Taco\Tools\Hockej\Core\Types;
use Taco\Tools\Hockej\Core\SourceContext;
use Taco\Tools\Hockej\Core\SplFileInfo;
use PHPUnit_Framework_TestCase;


/**
 * @call phpunit --bootstrap ../../../../../../bootstrap.php BuildscriptXmlParserTest.php
 */
class BuildscriptXmlParserTest extends PHPUnit_Framework_TestCase
{



	/**
	 *	Špatná data.
	 */
	public function testInvalidData()
	{
		$this->setExpectedException('Taco\Tools\Hockej\Core\ParseException');
		$parser = new BuildscriptXmlParser('nesmysl', $context = $this->getSourceContext());
	}



	/**
	 *	Žádné tasky v lokálním catalogu.
	 */
	public function testDataTasksInGlobalsEmpty()
	{
		$parser = new BuildscriptXmlParser(
				file_get_contents(__dir__ . '/patterns/build-minimal.hockej'),
				$context = $this->getSourceContext()
		);
		$tasks = $parser->getTasks();
		$this->assertCount(0, $tasks);

		//	Identification
		$id = $parser->getIdentification();
		$this->assertEmpty($id->id);
		$this->assertEmpty($id->version);
		$this->assertEmpty($id->description);
		$this->assertEquals($context, $id->sourceinfo);
	}



	/**
	 *	Task definovaný v Commandu.
	 */
	public function testLocalTaskInCommand()
	{
		$parser = new BuildscriptXmlParser(
				file_get_contents(__dir__ . '/patterns/build-1.hockej'),
				$context = $this->getSourceContext());
		$targets = $parser->getCommands();

		$this->assertCount(1, $targets);

		$this->markTestIncomplete('This test has not been implemented yet.');
		//	1
		$this->assertEquals('build', $targets[0]->name);
		$this->assertEquals('', $targets[0]->description);

		$this->assertCount(1, $targets[0]->lets, 'Definované lets v rámci lokálního targetu. Z venčí by nemělo jít vidět.');
		$this->assertEquals('dir.sourcex', $targets[0]->lets[0]->id);
		$this->assertEquals('path', $targets[0]->lets[0]->class);

		$this->assertCount(2, $targets[0]->lets[0]->content);

dump($targets[0]->lets[0]->content[0]);
die('====');
		$this->assertEquals('path', $targets[0]->lets[0]->content[0]->name);
		$this->assertEquals('ref', $targets[0]->lets[0]->content[0]->type);
		$this->assertEquals('dir.project', $targets[0]->lets[0]->content[0]->ref);

		$this->assertEquals('path', $targets[0]->lets[0]->content[1]->name);
		$this->assertEquals('inline', $targets[0]->lets[0]->content[1]->type);
		$this->assertEquals(array('filesets/a'), $targets[0]->lets[0]->content[1]->content);

	}




	/**
	 *	Lets definované v targetu.
	 */
	public function testDataTargetsInTasks()
	{
		$parser = new BuildscriptXmlParser(
				file_get_contents(__dir__ . '/patterns/build-1.hockej'),
				$context = $this->getSourceContext());
		$targets = $parser->getCommands();

		$this->markTestIncomplete('This test has not been implemented yet.');
		$this->assertCount(1, $targets);

		$this->assertCount(1, $targets[0]->tasks, 'Definované tasks v rámci lokálního targetu. Z venčí by nemělo jít vidět.');
		$this->assertEquals('package', $targets[0]->tasks[0]->extend);
		$this->assertNull($targets[0]->tasks[0]->name, 'Identifikátor pro odkazování z jiných míst.');

		// ... nekompletní.

		$this->assertEquals('file', $targets[0]->tasks[0]->content[2]->content[2]->name);
		$this->assertEquals('file', $targets[0]->tasks[0]->content[2]->content[2]->extend);
		$this->assertEquals(array('package.zip'), $targets[0]->tasks[0]->content[2]->content[2]->content);
	}



	/**
	 *	Identifikace katalogu
	 */
	public function testCatalogs()
	{
		$parser = new BuildscriptXmlParser(
				file_get_contents(__dir__ . '/patterns/build-1.hockej'),
				$context = $this->getSourceContext());
		$res = $parser->getCatalogs();

		$this->markTestIncomplete('This test has not been implemented yet.');
		$this->assertCount(2, $res);

		$this->assertEquals('core/filesystem', $res['fs']->from);
		$this->assertEquals('fs', $res['fs']->name);
		//~ $this->assertCount(2, $res['fs']->tasks);

		$this->assertNull($res['']->from);
		$this->assertNull($res['']->name);
		$this->assertCount(4, $res['']->tasks);
	}



	/**
	 *	Seznam globálních tasků.
	 */
	public function testBuildThreeCatalogs()
	{
		$parser = new BuildscriptXmlParser(
				file_get_contents(__dir__ . '/patterns/build-3.hockej'),
				$context = $this->getSourceContext());
		$catalogs = $parser->getCatalogs();

		$this->markTestIncomplete('This test has not been implemented yet.');
print_r($catalogs);
die('====');
		$catalog = $catalogs[''];
		$this->assertNull($catalog->name, 'U defaultního catalogu nemáme uvedené jméno.');
		$this->assertNull($catalog->from, 'U defaultního catalogu nemámý zdroj.');
		$this->assertCount(5, $catalog->tasks, 'Počet tasků v katalogu.');

		$task = $catalog->tasks[0];
		$this->assertEquals('dir-project', $task->name, 'Jméno Výrazu.');
		$this->assertEquals('path', $task->extend, 'Parent Výrazu.');
		$this->assertEquals(['./../../tests-data'], $task->content, 'Pojmenování Výrazu.');

		$task = $catalog->tasks[1];
		$this->assertEquals('dir-build', $task->name, 'Jméno Výrazu.');
		$this->assertEquals('path', $task->extend, 'Parent Výrazu.');
		$this->assertEquals(['./../../../temp'], $task->content, 'Pojmenování Výrazu.');

		$task = $catalog->tasks[2];
		$this->assertEquals('dir-source', $task->name, 'Jméno Výrazu.');
		$this->assertEquals('path', $task->extend, 'Parent Výrazu.');
		$this->assertEquals([
				(object) ['name' => 'path', 'extend' => 'dir-project', 'content' => []],
				(object) ['name' => 'path', 'extend' => 'path', 'content' => ['filesets/a']],
				], $task->content, 'Pojmenování Výrazu.');

		$task = $catalog->tasks[3];
		$this->assertEquals('fileset-source', $task->name, 'Jméno Výrazu.');
		$this->assertEquals('fileset', $task->extend, 'Parent Výrazu.');
		$this->assertEquals([
				(object) ['name' => 'source', 'extend' => 'dir-source', 'content' => []],
				(object) ['name' => 'include', 'extend' => 'file-mask', 'content' => ['**/*.php']],
				(object) ['name' => 'include', 'extend' => 'file-mask', 'content' => ['**/*.jpeg']],
				(object) ['name' => 'exclude', 'extend' => 'file-mask', 'content' => ['**/*~']],
				], $task->content);

		$task = $catalog->tasks[4];
		$this->assertEquals('abc', $task->name, 'Jméno Výrazu.');
		$this->assertEquals('package', $task->extend, 'Parent Výrazu.');
		$this->assertEquals((object)[
				'name' => 'type',
				'extend' => 'package-type',
				'content' => ['zip'],
				], $task->content[0]);
		$this->assertEquals((object)[
				'name' => 'source',
				'extend' => 'fileset-source',
				'content' => [],
				], $task->content[1]);
		$this->assertEquals((object)[
				'name' => 'destination',
				'extend' => 'file',
				'content' => [
						(object) ['name' => 'path', 'extend' => 'dir-build', 'content' => []],
						(object) ['name' => 'path', 'extend' => 'path', 'content' => ['temp/sample/data']],
						(object) ['name' => 'file', 'extend' => 'file', 'content' => ['package.zip']],
						],
				], $task->content[2]);
	}



	/**
	 *	Catalog pro příklad čtyři.
	 */
	public function testBuildFourCatalogs()
	{
		$parser = new BuildscriptXmlParser(
				file_get_contents(__dir__ . '/patterns/build-4.hockej'),
				$context = $this->getSourceContext());
		$list = $parser->getCatalogs();

		$this->assertCount(1, $list);

		$this->assertEquals('file://projects/catalogs/hockej/filesystem', $list['fs']->src);
		$this->assertEquals('fs', $list['fs']->name);
	}



	/**
	 *	Catalog pro příklad čtyři.
	 */
	public function testBuildFourCatalogsIdentifier()
	{
		$parser = new BuildscriptXmlParser(file_get_contents(__dir__ . '/patterns/build-4.hockej'), $context = $this->getSourceContext());
		$m = $parser->getIdentification();
		$this->assertEquals('hockej.sample.4', $m->id);
		$this->assertEquals('1.2.4', $m->version);
		$this->assertEquals('Lorem ipsum doler ist.', $m->description);
	}



	/**
	 *	Commands for example four.
	 */
	public function testBuildFourCommands()
	{
		$parser = new BuildscriptXmlParser(file_get_contents(__dir__ . '/patterns/build-4.hockej'), $context = $this->getSourceContext());
		$list = $parser->getCommands();
		$this->assertCount(1, $list);

		$cmd = reset($list);
		$this->assertCommandEquals("build", "", 1, $cmd);
		$this->assertTaskEquals("log", 'file-write', $cmd->tasks[0]);

		$this->assertTaskEquals("file", 'file', $cmd->tasks[0]->opts->file[0]);
		$this->assertEquals('/tmp/hockej.log', $cmd->tasks[0]->opts->file[0]->opts->default[0]);

		$this->assertTaskEquals("content", 'string*', $cmd->tasks[0]->opts->content[0]);
		$this->assertEquals('Hello, I\'am Hockej!', $cmd->tasks[0]->opts->content[0]->opts->default[0]);
	}



	/**
	 *	Ukázkový build 1.hello-world
	 */
	public function testSampleV1()
	{
		$parser = new BuildscriptXmlParser(
				file_get_contents(__dir__ . '/patterns/1.hello-world/build.hockej'),
				$context = $this->getSourceContext());
		$tasks = $parser->getTasks();

		$id = $parser->getIdentification();
		$this->assertEmpty($id->id);
		$this->assertEmpty($id->version);
		$this->assertEquals("## Sample 1", trim($id->description));

		$tasks = $parser->getTasks();
		$this->assertCount(0, $tasks);

		$commands = $parser->getCommands();
		$this->assertCount(1, $commands);
		$command = reset($commands);
		$this->assertCommandEquals("build", "", 1, $command);
		$task = reset($command->tasks);
		$this->assertTaskEquals("print", 'hockej://build-in/hockej/filesystem:0.0.4:file-write', $task);
		$this->assertEquals(['file', 'content'], array_keys((array)$task->opts));
		$this->assertTaskEquals("file", 'hockej://build-in/hockej/filesystem:0.0.4:file', $task->opts->file[0]);

		$this->assertTaskEquals("root", 'hockej://build-in/hockej/filesystem:0.0.4:path', $task->opts->file[0]->opts->root[0]);
		$this->assertEquals('.', $task->opts->file[0]->opts->root[0]->opts->default[0]);

		$this->assertTaskEquals("content", 'hockej://build-in/hockej/core:0.0.1:string', $task->opts->content[0]);
		$this->assertEquals('Hello, I\'am Hockej!', $task->opts->content[0]->opts->default[0]);

		$catalogs = $parser->getCatalogs();
		$this->assertCount(2, $catalogs);
		$this->assertEquals((object) array(
				'src' => 'hockej://build-in/hockej/filesystem',
				'name' => 'fs',
				'version' => '0.0.4',
				'file' => new SplFileInfo('.', '.'),
				), $catalogs['fs']);
		$this->assertEquals((object) array(
				'src' => 'hockej://build-in/hockej/core',
				'name' => 'core',
				'version' => '0.0.1',
				'file' => new SplFileInfo('.', '.'),
				), $catalogs['core']);
	}



	/**
	 *	Ukázkový build 2.mkdir
	 */
	public function testSample2Mkdir()
	{
		$parser = new BuildscriptXmlParser(
				file_get_contents(__dir__ . '/patterns/2.mkdir/build.hockej'),
				$context = $this->getSourceContext());
		$tasks = $parser->getTasks();

		//	Identification
		$id = $parser->getIdentification();
		$this->assertEmpty($id->id);
		$this->assertEmpty($id->version);
		$this->assertEquals("## Sample 2", trim($id->description));

		//	Tasks
		$tasks = $parser->getTasks();
		$this->assertCount(3, $tasks);

		//~ print_r($tasks['path-source']->getSourceInfo());
		//		<path-source from="path">source</path-source>
		$this->assertEquals((object) array(
				'code' => '<path-source from="path">source</path-source>',
				'context' => $context,
				'line' => 17,
				), $tasks['path-source']->getSourceInfo());

		$this->assertTaskEquals('path-source', 'path', $tasks['path-source']);
		$this->assertEquals('source', $tasks['path-source']->opts->default[0]);

		$this->assertTaskEquals('path-test', 'path', $tasks['path-test']);
		$this->assertCount(2, $tasks['path-test']->opts->path);
		$this->assertTaskEquals('path', 'path-source', $tasks['path-test']->opts->path[0]);
		$this->assertNull($tasks['path-test']->opts->path[0]->opts);
		$this->assertTaskEquals('path', 'string', $tasks['path-test']->opts->path[1]);
		$this->assertEquals('/tmp/foo', $tasks['path-test']->opts->path[1]->opts->default[0]);

		$this->assertTaskEquals('make-var-dir', 'path-create', $tasks['make-var-dir']);
		$this->assertTaskEquals('path', 'path-test', $tasks['make-var-dir']->opts->path[0]);

		$this->assertNull($tasks['make-var-dir']->opts->path[0]->opts);

		//	Commands
		$commands = $parser->getCommands();
		$this->assertCount(1, $commands);
		$command = reset($commands);
		$this->assertCommandEquals("build", "", 1, $command);
		$task = reset($command->tasks);
		$this->assertTaskEquals("make-var-dir", 'make-var-dir', $task);
		$this->assertNull($task->opts);

		$info = $commands[0]->getSourceInfo();
		$this->assertEquals("<command name=\"build\">\n\t\t<make-var-dir from=\"make-", substr($info->code, 0, 50));
		$this->assertEquals($context, $info->context);
		$this->assertEquals(32, $info->line);

		//	Catalogs
		$catalogs = $parser->getCatalogs();
		$this->assertCount(2, $catalogs);
		$this->assertEquals((object) array(
				'src' => 'hockej://build-in/hockej/filesystem',
				'version' => '0.0.4',
				'name' => 'fs',
				'file' => new SplFileInfo('.', '.'),
				), $catalogs['fs']);
		$this->assertEquals((object) array(
				'src' => 'hockej://build-in/hockej/core',
				'version' => '0.0.1',
				'name' => 'core',
				'file' => new SplFileInfo('.', '.'),
				), $catalogs['core']);
	}


	/**
	 * Příklad 7.buildscript-with-runtime
	 */
	function testSample7BuildscriptWithRuntime()
	{
		$this->markTestIncomplete('This test has not been implemented yet.');

		$parser = new BuildscriptXmlParser(
				file_get_contents(__dir__ . '/patterns/7.buildscript-with-runtime/build.hockej'),
				$context = $this->getSourceContext()
		);

		//	Identification
		$id = $parser->getIdentification();
		$this->assertEmpty($id->id);
		$this->assertEmpty($id->version);
		$this->assertEquals("## Sample 7", substr(trim($id->description), 0, 11));
		$this->assertEquals($context, $id->sourceinfo);

		//	Tasks
		$tasks = $parser->getTasks();
		$this->assertCount(1, $tasks);
		$this->assertTaskRuntimeEquals('php', $context, 'boo/bootstrap.php', $tasks['print']);
	}


	//------------------------------------------------------------------


	private function getRoot()
	{
		return realpath(__dir__ . '/../../../../../../../');
	}



	private function assertCommandEquals($name, $description, $taskCount, $source)
	{
		$this->assertEquals($name, $source->name);
		$this->assertEquals($description, $source->description);
		$this->assertCount($taskCount, $source->tasks);
	}



	private function assertTaskEquals($name, $extend, $source)
	{
		$this->assertEquals($name, $source->name);
		$this->assertEquals($extend, $source->extend);
	}


	/**
	 * Task, který je vytvořený z nějakého jiného tasku. Neobsahuje runtime.
	 */
	private function assertCompositeTaskEquals($name, $extend, $description, Domains\Task $source)
	{
		$this->assertEquals($name, $source->name);
		$this->assertEquals($extend, $source->extend);
		$this->assertEquals($description, $source->description);
		$this->assertNull($source->runtime, 'Skládaný task nemůže obsahovat runtime.');
	}


	/**
	 * Task, který je vytvořený z nějakého jiného tasku. Neobsahuje runtime.
	 */
	private function assertRuntimeTaskEquals($name, $extend, $description, Domains\Task $source)
	{
		$this->assertEquals($name, $source->name);
		$this->assertEquals($extend, $source->extend);
		$this->assertEquals($description, $source->description);
		$this->assertNotNull($source->runtime, 'Skládaný task nemůže obsahovat runtime.');
	}


	private function assertTaskRuntimeEquals($runtime, $context, $bootstrap, $source)
	{
		$this->assertEquals($runtime, $source->runtime->runtime, 'Jazyk runtime.');
		$this->assertEquals($context, $source->runtime->context, 'Umístění adresáře s definicí catalogu, nebo od kterého se počítá cesta k bootstrapu.');
		$this->assertEquals($bootstrap, $source->runtime->bootstrap, 'Cesta k bootstrapu.');
	}


	private function getSourceContext()
	{
		return new SourceContext(
				new SplFileInfo(__file__, '.')
				);
	}


}
