<?php
/**
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 * Copyright (c) since 2004 Martin Takáč
 * @author Martin Takáč <martin@takac.name>
 */

namespace Taco\Tools\Hockej\Core\Parsers;

use Taco\Tools\Hockej\Core\Types;
use Taco\Tools\Hockej\Core\SourceContext;
use Taco\Tools\Hockej\Core\SplFileInfo;
use PHPUnit_Framework_TestCase;


/**
 * @call phpunit CatalogXmlParserTest.php
 */
class CatalogXmlParserTest extends PHPUnit_Framework_TestCase
{



	/**
	 *	Špatná data.
	 */
	function testInvalidData()
	{
		$this->setExpectedException('Taco\Tools\Hockej\Core\ParseException');
		$parser = new CatalogXmlParser('nesmysl', $this->getSourceContext('x'));
	}



	/**
	 *	Identifikace katalogu
	 */
	function testIdentification()
	{
		$parser = new CatalogXmlParser(
				file_get_contents(__dir__ . '/patterns/catalog-simple.hockej'),
				$context = $this->getSourceContext('patterns/catalog-simple.hockej')
		);
		$res = $parser->getIdentification();
		$this->assertEquals('hockej.core', $res->id);
		$this->assertEquals('hockej', $res->vendor, 'Implementator.');
		$this->assertEquals('core', $res->package, 'Balíček funkcionality.');
		$this->assertEquals('0.0.2', $res->version, 'Verze katalogu.');
		$this->assertEquals(array('std'), $res->categories, 'Zařazení do nějakých katgorií');
		$this->assertEquals('Základní knihovna typů, filterů a tasků.', $res->description, 'Lidský popis katalogu.');
		$this->assertEquals($context, $res->sourceinfo);
	}



	/**
	 *	Co katalog obsahuje.
	 */
	function testDefinitions()
	{
		$parser = new CatalogXmlParser(
				file_get_contents(__dir__ . '/patterns/catalog-simple.hockej'),
				$context = $this->getSourceContext('patterns/catalog-simple.hockej')
		);
		$res = $parser->getDefinitions();

		$this->assertCount(1, $res);
		$this->assertTaskEquals('path', 'self', $res['path']);
		$this->assertTaskEquals('root', 'path', $res['path']->opts->root[0]);
		$this->assertNull($res['path']->opts->root[0]->opts);
		$this->assertTaskEquals('path', 'path', $res['path']->opts->path[0]);
		$this->assertNull($res['path']->opts->path[0]->opts);
		$this->assertTaskRuntimeEquals('php', $context, 'bootstrap.php', $res['path']);
	}



	function testCatalogs()
	{
		$parser = new CatalogXmlParser(
				file_get_contents(__dir__ . '/patterns/catalog-simple.hockej'),
				$context = $this->getSourceContext('patterns/catalog-simple.hockej')
				);
		$catalogs = $parser->getCatalogs();
		$this->assertCount(1, $catalogs);
		$this->assertEquals((object) array(
				'src' => 'hockej://build-in/hockej/core',
				'name' => 'core',
				'version' => '0.0.81',
				'file' => SplFileInfo::create(__dir__, 'patterns/catalog-simple.hockej'),
				), $catalogs['core']);
	}



	function testDefinitionsDepends()
	{
		$parser = new CatalogXmlParser(
				file_get_contents(__dir__ . '/patterns/catalog-with-depends.hockej'),
				$context = $this->getSourceContext('patterns/catalog-with-depends.hockej')
				);
		$res = $parser->getDefinitions();

		$this->assertCount(1, $res, 'Počet tasků k dispozici.');

		$this->assertEquals('el', $res['el']->getName(), 'Jméno tasku.');
		$this->assertEquals('self', $res['el']->getExtend(), 'Předloha');
		$this->assertEquals('com', $res['el']->getDescription(), 'Popis tasku.');
		$this->assertEquals('foo', $res['el']->getDefaultOptionName(), 'Který option bude použit, když nebude explicitně určeno jeho jméno.');
		//~ $this->assertEquals('self', $res['el']->getMinOccurs(), '! v tomto případě nesmí být použit.');
		//~ $this->assertEquals('self', $res['el']->getMaxOccurs(), '! v tomto případě nesmí být použit.');

		$opts = $res['el']->getOptions();

		$this->assertEquals(array('foo', 'boo'), array_keys((array)$opts), 'Které optiona task má.');

		$this->assertCount(1, $opts->foo, 'Počet prvků tohoto jména.');

		$res = $opts->foo[0];
		$this->assertEquals('foo', $res->getName(), 'Jméno tasku.');
		$this->assertEquals('el', $res->getExtend(), 'Předloha');
		$this->assertEquals('com2', $res->getDescription(), 'Popis tasku.');
		$this->assertNull($res->getDefaultOptionName(), 'Který option bude použit, když nebude explicitně určeno jeho jméno.');

		$res = $opts->boo[0];
		$this->assertEquals('boo', $res->getName(), 'Jméno tasku.');
		$this->assertEquals('hockej://build-in/hockej/core:0.0.81:string', $res->getExtend(), 'Předloha');
		$this->assertEquals('com3', $res->getDescription(), 'Popis tasku.');
		$this->assertNull($res->getDefaultOptionName(), 'Který option bude použit, když nebude explicitně určeno jeho jméno.');
	}



	// -----------------------------------------------------------------



	private function assertTaskEquals($name, $extend, $source)
	{
		$this->assertEquals($name, $source->name);
		$this->assertEquals($extend, $source->extend);
	}


	/**
	 * Task, který je vytvořený z nějakého jiného tasku. Neobsahuje runtime.
	 */
	private function assertCompositeTaskEquals($name, $extend, $description, Domains\Task $source)
	{
		$this->assertEquals($name, $source->name);
		$this->assertEquals($extend, $source->extend);
		$this->assertEquals($description, $source->description);
		$this->assertNull($source->runtime, 'Skládaný task nemůže obsahovat runtime.');
	}


	/**
	 * Task, který je vytvořený z nějakého jiného tasku. Neobsahuje runtime.
	 */
	private function assertRuntimeTaskEquals($name, $extend, $description, Domains\Task $source)
	{
		$this->assertEquals($name, $source->name);
		$this->assertEquals($extend, $source->extend);
		$this->assertEquals($description, $source->description);
		$this->assertNotNull($source->runtime, 'Skládaný task nemůže obsahovat runtime.');
	}


	private function assertTaskRuntimeEquals($runtime, $context, $bootstrap, $source)
	{
		$this->assertEquals($runtime, $source->runtime->runtime, 'Jazyk runtime.');
		$this->assertEquals($context, $source->runtime->context, 'Umístění adresáře s definicí catalogu, nebo od kterého se počítá cesta k bootstrapu.');
		$this->assertEquals($bootstrap, $source->runtime->bootstrap, 'Cesta k bootstrapu.');
	}


	private function getSourceContext($file)
	{
		return new SourceContext(
				SplFileInfo::create(__dir__, $file)
		);
	}

}
