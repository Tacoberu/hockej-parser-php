<?php
/**
 * This file is part of the Taco Projects.
 *
 * Copyright (c) 2004, 2013 Martin Takáč (http://martin.takac.name)
 *
 * For the full copyright and license information, please view
 * the file LICENCE that was distributed with this source code.
 *
 * PHP version 5.3
 *
 * @author     Martin Takáč (martin@takac.name)
 */


namespace Taco\Tools\Hockej\Core\Parsers;


require_once __dir__ . '/../../../../../../../vendor/autoload.php';


use Taco\Tools\Hockej\Core\Types;
use Taco\Tools\Hockej\Core\Domains;
use Taco\Tools\Hockej\Core\SourceContext;
use Taco\Tools\Hockej\Core\SplFileInfo;
use PHPUnit_Framework_TestCase;


/**
 * @call phpunit CommonXmlParserTest.php
 */
class CommonXmlParserTest extends PHPUnit_Framework_TestCase
{


	/**
	 * Single task - neplatné atributy. Chybí from.
	 */
	function testSingleTaskInvalidMissingFrom()
	{
		$this->setExpectedException('Taco\Tools\Hockej\Core\ParseException',
				"Required attr `from' is not set in: `<?xml version=\"1.0\"?>\n"
					. "<task ref=\"package\">ahoj</task>\n' in line: 1.");
		$source = '<task ref="package">ahoj</task>';
		$parser = new CommonXmlParser(new SourceContext(
				new SplFileInfo(__file__, '.')
				));
		$res = $parser->buildTaskDefinition(simplexml_load_string($source));
	}


	/**
	 * Single task - neplatné atributy. Chybí from.
	 */
	function testSingleTaskInvalidEmptyFrom()
	{
		$this->setExpectedException('Taco\Tools\Hockej\Core\ParseException',
				"Required attr `from' is empty in: `<?xml version=\"1.0\"?>\n"
					. "<task from=\"\">ahoj</task>\n' in line: 1.");
		$source = '<task from="">ahoj</task>';
		$parser = new CommonXmlParser(new SourceContext(
				new SplFileInfo(__file__, '.')
				));
		$res = $parser->buildTaskDefinition(simplexml_load_string($source));
	}


	/**
	 * Single task - neplatné atributy. Chybí from.
	 */
	function testSingleRuntimeTask()
	{
		$source = '<dump from="self"
				implement-runtime="php" implement-bootstrap="bootstrap.php"
				description="Element pro odkazování cesty." />';
		$context = $this->getSourceContext();
		$parser = new CommonXmlParser($context);
		$res = $parser->buildTaskDefinition(simplexml_load_string($source));

		$this->assertRuntimeTaskEquals('dump', 'self', "Element pro odkazování cesty.", $res);
		$this->assertTaskRuntimeEquals('php', $context, 'bootstrap.php', $res);
	}



	/**
	 * Single task.
	 */
	function testSingleTask()
	{
		$source = '<foo from="package">ahoj</foo>';
		$parser = new CommonXmlParser($context = new SourceContext(
				new SplFileInfo(__file__, '.')
				));
		$res = $parser->buildTaskDefinition(simplexml_load_string($source));
		$this->assertCompositeTaskEquals('foo', 'package', Null, $res);
		$this->assertEquals((object) array('default' => ['ahoj']), $res->options);
		$this->assertEquals($context, $res->sourceInfo->context);
		$this->assertEquals("<?xml version=\"1.0\"?>\n<foo from=\"package\">ahoj</foo>\n", $res->sourceInfo->code);
	}


	/**
	 *
	 */
	function testTskWithArgs()
	{
		$node = simplexml_load_string('<foo from="package" description="abc">
	 			<type from="string">zip</type>
	 			<source from="fileset.source"/>
	 			<destination from="file">
	 				<path from="dir.build"/>
	 				<path from="path">temp/sample/data</path>
	 				<file from="string">package.zip</file>
	 			</destination>
	 		</foo>');
		$parser = new CommonXmlParser(new SourceContext(
				new SplFileInfo(__file__, '.')
				));
		$res = $parser->buildTaskDefinition($node, array(
				'fileset' => (object) array(
						'src' => 'hockej://build-in/hockej/fileset',
						'name' => "fileset",
						'version' => "0.0.1",
						),
				'dir' => (object) array(
						'src' => 'hockej://build-in/hockej/dir',
						'name' => "dir",
						'version' => "0.0.3",
						),
				));

		$this->assertCompositeTaskEquals('foo', 'package', 'abc', $res);
		$this->assertCount(3, (array)$res->options);
		$this->assertCount(1, (array)$res->options->type);
		$this->assertCount(1, (array)$res->options->source);
		$this->assertCount(1, (array)$res->options->destination);

		$this->assertCompositeTaskEquals('type', 'string', Null, $res->options->type[0]);
		$this->assertEquals((object) array('default' => ['zip']), $res->options->type[0]->options);
		$this->assertCompositeTaskEquals('source', 'hockej://build-in/hockej/fileset:0.0.1:source', Null, $res->options->source[0]);
		$this->assertEquals("<source from=\"fileset.source\"/>", $res->options->source[0]->sourceInfo->code);
		$this->assertEquals(3, $res->options->source[0]->sourceInfo->line);
		$this->assertNull($res->options->source[0]->options);
		$this->assertCompositeTaskEquals('destination', 'file', Null, $res->options->destination[0]);
	}



	function testBuildTaskDef()
	{
		$node = simplexml_load_string('<type from="string" param-as="for">zip</type>');
		$parser = new CommonXmlParser(new SourceContext(
				new SplFileInfo(__file__, '.')
				));
		$res = $parser->buildTaskDefinition($node);
		$this->assertEquals('type', $res->getName());
		$this->assertEquals('string', $res->getExtend());
		$this->assertEquals(array('for'), $res->getRefs());
		$this->assertEquals(array('zip'), $res->getOptions()->default);
		$this->assertNull($res->getRuntime());
	}



	function testBuildTaskDef2()
	{
		$node = simplexml_load_string('<foo from="c.blr">
				<type from="string" param-as="for">zip</type>
			</foo>');
		$parser = new CommonXmlParser(new SourceContext(
				new SplFileInfo(__file__, '.')
				));
		$res = $parser->buildTaskDefinition($node, array(
				'c' => (object) array(
						'src' => 'hockej://build-in/hockej/core',
						'name' => "c",
						'version' => "0.0.81",
						),
				));
		$this->assertEquals('foo', $res->getName());
		$this->assertEquals('type', $res->getOptions()->type[0]->getName());
		$this->assertEquals(['for'], $res->getOptions()->type[0]->getRefs());
	}



	function testBuildTaskDef3()
	{
		$node = simplexml_load_string('<foo from="c.blr">
				<boo from="a"/>
				<boo from="c.frr">
					<axim from="a"/>
					<type from="string" param-as="for">zip</type>
				</boo>
				<taxim from="a"/>
			</foo>');
		$parser = new CommonXmlParser(new SourceContext(
				new SplFileInfo(__file__, '.')
				));
		$res = $parser->buildTaskDefinition($node, array(
				'c' => (object) array(
						'src' => 'hockej://build-in/hockej/core',
						'name' => "c",
						'version' => "0.0.81",
						),
				));
		$this->assertEquals('foo', $res->getName());
		$this->assertEquals('type', $res->getOptions()->boo[1]->getOptions()->type[0]->getName());
		$this->assertEquals(['for'], $res->getOptions()->boo[1]->getOptions()->type[0]->getRefs());
	}



	function testBuildTaskDef2d()
	{
		$node = simplexml_load_string('<foo from="c.blr">
				<type from="string">zip</type>
				<type from="string" param-as="for"/>
			</foo>');
		$parser = new CommonXmlParser(new SourceContext(
				new SplFileInfo(__file__, '.')
				));
		$res = $parser->buildTaskDefinition($node, array(
				'c' => (object) array(
						'src' => 'hockej://build-in/hockej/core',
						'name' => "c",
						'version' => "0.0.81",
						),
				));
		$this->assertEquals('foo', $res->getName());
		$this->assertEquals('type', $res->getOptions()->type[1]->getName());
		$this->assertEquals(['for'], $res->getOptions()->type[1]->getRefs());
	}


	function testBuildTaskDef2dd()
	{
		$node = simplexml_load_string('<foo from="c.blr">
				<type from="string" param-as="ro">zip</type>
				<type from="string" param-as="for">rar</type>
			</foo>');
		$parser = new CommonXmlParser(new SourceContext(
				new SplFileInfo(__file__, '.')
				));
		$res = $parser->buildTaskDefinition($node, array(
				'c' => (object) array(
						'src' => 'hockej://build-in/hockej/core',
						'name' => "c",
						'version' => "0.0.81",
						),
				));
		$this->assertEquals('foo', $res->getName());
		$this->assertEquals('hockej://build-in/hockej/core:0.0.81:blr', $res->getExtend());
		$this->assertEquals('type', $res->getOptions()->type[0]->getName());
		$this->assertEquals(['ro'], $res->getOptions()->type[0]->getRefs());
		$this->assertEquals('type', $res->getOptions()->type[1]->getName());
		$this->assertEquals(['for'], $res->getOptions()->type[1]->getRefs());
		$this->assertNull($res->getDefaultOptionName());
	}



	function _testDefaultOptionNameAndMaxAndMinOccurs()
	{
		$node = simplexml_load_string('<foo description="abc" from="self"
					implement-runtime="php" implement-bootstrap="boo/bootstrap.php"
					default="deux"
					return="bono">
				<une from="string" param-as="root" min-occurs="0" max-occurs="1"/>
				<deux from="string" param-as="root" min-occurs="1" max-occurs="4">blah</deux>
	 		</foo>');
		$parser = new CommonXmlParser(new SourceContext(
				new SplFileInfo(__file__, '.')
				));
		$res = $parser->buildTaskDefinition($node);
		$this->assertEquals('foo', $res->getName());
		$this->assertEquals('self', $res->getExtend());
		$this->assertEquals('abc', $res->getDescription());
		$this->assertEquals('deux', $res->getDefaultOptionName());
		$this->assertEquals('bono', $res->getReturnType());

		$this->assertEquals('une', $res->getOptions()->une[0]->getName());
		$this->assertNull($res->getOptions()->une[0]->getDefaultOptionName());
		$this->assertEquals(0, $res->getOptions()->une[0]->getMinOccurs());
		$this->assertEquals(1, $res->getOptions()->une[0]->getMaxOccurs());

		$this->assertEquals('deux', $res->getOptions()->deux[0]->getName());
		$this->assertNull($res->getOptions()->deux[0]->getDefaultOptionName());
		$this->assertEquals(1, $res->getOptions()->deux[0]->getMinOccurs());
		$this->assertEquals(4, $res->getOptions()->deux[0]->getMaxOccurs());
	}


	// -----------------------------------------------------------------


	/**
	 * Task, který je vytvořený z nějakého jiného tasku. Neobsahuje runtime.
	 */
	private function assertCompositeTaskEquals($name, $extend, $description, Domains\Task $source)
	{
		$this->assertEquals($name, $source->name);
		$this->assertEquals($extend, $source->extend);
		$this->assertEquals($description, $source->description);
		$this->assertNull($source->runtime, 'Skládaný task nemůže obsahovat runtime.');
	}


	/**
	 * Task, který je vytvořený z nějakého jiného tasku. Neobsahuje runtime.
	 */
	private function assertRuntimeTaskEquals($name, $extend, $description, Domains\Task $source)
	{
		$this->assertEquals($name, $source->name);
		$this->assertEquals($extend, $source->extend);
		$this->assertEquals($description, $source->description);
		$this->assertNotNull($source->runtime, 'Skládaný task nemůže obsahovat runtime.');
	}



	private function assertTaskRuntimeEquals($runtime, $context, $bootstrap, $source)
	{
		$this->assertEquals($runtime, $source->runtime->runtime, 'Jazyk runtime.');
		$this->assertEquals($context, $source->runtime->context, 'Umístění adresáře s definicí catalogu, nebo od kterého se počítá cesta k bootstrapu.');
		$this->assertEquals($bootstrap, $source->runtime->bootstrap, 'Cesta k bootstrapu.');
	}



	private function getSourceContext()
	{
		return new SourceContext(
				new SplFileInfo(__file__, '.')
				);
	}

}
