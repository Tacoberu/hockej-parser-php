<?php
/**
 * For the full copyright and license information, please view
 * the file LICENCE that was distributed with this source code.
 * Copyright (c) since 2004 Martin Takáč
 * @author Martin Takáč <martin@takac.name>
 */

namespace Taco\Tools\Hockej\Core\Parsers;

use Taco\Tools\Hockej\Core\Types;
use Taco\Tools\Hockej\Core\SourceContext;
use Taco\Tools\Hockej\Core\SplFileInfo;
use PHPUnit_Framework_TestCase;


/**
 * @call phpunit CatalogXmlParserVer2Test.php
 */
class CatalogXmlParserVer2Test extends PHPUnit_Framework_TestCase
{


	/**
	 *	Špatná data.
	 */
	function testInvalidData()
	{
		$this->setExpectedException('Taco\Tools\Hockej\Core\ParseException');
		$parser = new CatalogXmlParser('nesmysl', $this->getSourceContext());
	}



	/**
	 *	Identifikace katalogu
	 */
	function testIdentification()
	{
		$parser = new CatalogXmlParser(
				file_get_contents(__dir__ . '/patterns/version2/catalog.hockej'),
				$context = $this->getSourceContext()
				);
		$res = $parser->getIdentification();

		$this->assertEquals('hockej.samples', $res->id, 'deprecated');
		$this->assertEquals('hockej', $res->vendor, 'Implementator.');
		$this->assertEquals('core', $res->package, 'Balíček funkcionality.');
		$this->assertEquals('0.1', $res->version, 'Verze katalogu.');
		$this->assertEquals(array('std'), $res->categories, 'Zařazení do nějakých katgorií');
		$this->assertEquals('Základní knihovna typů.', $res->description, 'Lidstký popis katalogu.');
		$this->assertEquals($context, $res->sourceinfo);
	}



	/**
	 *	Co katalog obsahuje za tasky.
	 */
	function testTasks()
	{
		$parser = new CatalogXmlParser(
				file_get_contents(__dir__ . '/patterns/version2/catalog.hockej'),
				$context = $this->getSourceContext()
				);
		$res = $parser->getDefinitions();

		$this->assertCount(5, $res);
		$this->assertTaskEqualsSignature('plus', 'self', 'Sečtení n čísel.', 'value', 'core.numeric/hockej:0:1{numeric,plus,minus}', 0, Null, $res['plus']);
		$this->assertTaskEqualsSignature('minus', 'self', 'Odečtení n čísel.', Null, 'core.numeric/hockej:0:1{numeric,plus,minus}', 0, Null, $res['minus']);
		$this->assertTaskEqualsSignature('pi', 'self', Null, Null, 'hockej://build-in/hockej/core:0.0.81#real', 0, Null, $res['pi']);
		$this->assertTaskEqualsSignature('hello', 'self', Null, Null, Null, 0, Null, $res['hello']);
		$this->assertTaskEqualsSignature('numeric', 'self', 'Vytvoření instance čísla', ':literal', 'core.numeric/hockej:0:1{numeric,plus,minus}', 0, Null, $res['numeric']);

		$this->assertTaskEqualsSignature('value', 'numeric', Null, Null, Null, 0, Null, reset($res['plus']->options->value));
	}



	/**
	 *	Co katalog obsahuje za symboly.
	 */
	function testSymbols()
	{
		$parser = new CatalogXmlParser(
				file_get_contents(__dir__ . '/patterns/version2/catalog.hockej'),
				$context = $this->getSourceContext()
				);
		$res = $parser->getSymbols();
		$this->assertCount(1, $res);
		$this->assertEquals('core.numeric/hockej:0:1{numeric,plus,minus}', (string)$res['numeric']);
	}



	/**
	 * Jaké catalogy linkuje.
	 * @deprecated
	 */
	function testCatalogs()
	{
		$parser = new CatalogXmlParser(
				file_get_contents(__dir__ . '/patterns/catalog-simple.hockej'),
				$context = $this->getSourceContext()
				);
		$catalogs = $parser->getCatalogs();
		$this->assertCount(1, $catalogs);
		$this->assertEquals((object) array(
				'src' => 'hockej://build-in/hockej/core',
				'name' => 'core',
				'version' => '0.0.81',
				'file' => new SplFileInfo('.', '.'),
				), $catalogs['core']);
	}



	// -----------------------------------------------------------------



	private function assertTaskEqualsSignature($name, $extend, $description, $default, $return, $min, $max, $source)
	{
		$this->assertEquals($name, $source->name, 'Name of task.');
		$this->assertEquals($extend, $source->extend, 'Extend from task.');
		$this->assertEquals($description, $source->description, 'Description of task.');
		$this->assertEquals($default, $source->defaultOptionName, 'Default option of task.');
		$this->assertEquals($return, (string)$source->returnType, 'Return of task.');
		$this->assertEquals($min, $source->minOccurs, 'Min Occurs of task.');
		$this->assertEquals($max, $source->maxOccurs, 'Min Occurs of task.');
	}



	/**
	 * Task, který je vytvořený z nějakého jiného tasku. Neobsahuje runtime.
	 */
	private function assertCompositeTaskEquals($name, $extend, $description, Domains\Task $source)
	{
		$this->assertEquals($name, $source->name);
		$this->assertEquals($extend, $source->extend);
		$this->assertEquals($description, $source->description);
		$this->assertNull($source->runtime, 'Skládaný task nemůže obsahovat runtime.');
	}


	/**
	 * Task, který je vytvořený z nějakého jiného tasku. Neobsahuje runtime.
	 */
	private function assertRuntimeTaskEquals($name, $extend, $description, Domains\Task $source)
	{
		$this->assertEquals($name, $source->name);
		$this->assertEquals($extend, $source->extend);
		$this->assertEquals($description, $source->description);
		$this->assertNotNull($source->runtime, 'Skládaný task nemůže obsahovat runtime.');
	}


	private function assertTaskRuntimeEquals($runtime, $context, $bootstrap, $source)
	{
		$this->assertEquals($runtime, $source->runtime->runtime, 'Jazyk runtime.');
		$this->assertEquals($context, $source->runtime->context, 'Umístění adresáře s definicí catalogu, nebo od kterého se počítá cesta k bootstrapu.');
		$this->assertEquals($bootstrap, $source->runtime->bootstrap, 'Cesta k bootstrapu.');
	}


	private function getSourceContext()
	{
		return new SourceContext(
				new SplFileInfo(__file__, '.')
				);
	}

}
